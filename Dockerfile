FROM eclipse-temurin:21.0.2_13-jdk
LABEL maintainer="Intro_JavaSpring"
COPY java_spring.jar /app/java_spring.jar
WORKDIR "/app"
CMD ["java", "-jar", "/app/java_spring.jar"]
